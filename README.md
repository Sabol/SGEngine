## SGEngine - The Engine behind the game I am going to finish one of these days, Sword Guy.

## TODO:
1. ~~Implement the GameObjClass.~~
2. Decouple functionality.
3. Continue to just build on features as I need them for Sword Guy.
4. Fix the issue with the mouse cursor supplying incorrect coordinates. (Will I ever get to this?)
5. Label all globals as gloab with the suffix "g_".
6. SO MANY OTHER THINGS
7. ~~Test vector math lib fully.~~ 
8. Implement SAT collision (This is a pretty massive change)
