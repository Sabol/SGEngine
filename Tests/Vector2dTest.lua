package.path = package.path .. ";../Engine/Physics/?.lua;"
-- print( package.path )
require "Vector2d"
--[[
Things to test:
    All the methods:
        new
        init
        add
        dot
        magnitude
        project
        normal

        Remember to try and break the code as well as test /normal/ use cases
--]]

-- Allow the user to input two vectors
print( 'Enter the x and y of u: ')
uX = io.read()
uY = io.read()
print( 'Enter the x and y of v: ')
vX = io.read()
vY = io.read()

-- Initialze two vectors
u = Vector2d.new()
u:init( uX, uY )

v = Vector2d.new()
v:init( vX, vY )

-- Test normal use cases
print( 'u x: '..u.x..' y: '..u.y )
print( 'v x: '..v.x..' y: '..v.y )

--[[
Test vector addition
u = <1,5> v = <3,2>
u + v = <4,7>
--]]
w = u:add( v )
print( 'u + v = ' .. '<' .. w.x .. ',' .. w.y .. '>' )

--[[
Test vector dot product
u dot v = 13
--]]
dot = u:dot( v )
print( 'dot product of u and v = ' .. dot )

--[[
Test vector magnitude
||u|| approx = 5.098
--]]
mag_u = u:magnitude()
mag_v = v:magnitude()
print( 'magnitude of u = ' .. mag_u )
print( 'magnitude of v = ' .. mag_v )

--[[
Test vector projection
project u onto v = <1.5, 1>
--]]
proj_u_v = u:project( v )
proj_v_u = v:project( u )
print( 'projection of u onto v = <' .. proj_u_v.x .. ',' .. proj_u_v.y .. '>' )
print( 'projection of v onto u = <' .. proj_v_u.x .. ',' .. proj_v_u.y .. '>' )

--[[
Test vector normal
--]]
print( 'Enter x and y of point a: ')
a = {}
a.x = io.read()
a.y = io.read()
print( 'Enter x and y of point b: ')
b = {}
b.x = io.read()
b.y = io.read()

normal = Vector2d:normal( a, b )

print( 'Normal ab: <'..normal.x..','..normal.y..'>' )

unit = u:unit()
print( 'Unit vector of u: <'..unit.x..','..unit.y..'>' )
unit = v:unit()
print( 'Unit vector of v: <'..unit.x..','..unit.y..'>' )
