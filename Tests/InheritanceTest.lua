-- Test out an inheritance pattern in lua
package.path = package.path .. ";../Engine/Helpers/?.lua;"

require 'Inherit'

-- BaseClass ===================================================================
BaseClass = inherit( nil )

function BaseClass:init()
    self.name = 'BaseClass'
    self.id = '1234567890'
    self.someString = 'hey I am a base class!'
end

function BaseClass:getName()
    return self.name
end

function BaseClass:getId()
    return self.id
end

function BaseClass:getSomeString()
    return self.someString
end

-- ChildClass ==================================================================
ChildClass = inherit( BaseClass ) -- Create a new class that inherits from BaseClass

function ChildClass:init()
    self:superClass():init()
    self.street = 'Someplace 17828 USA town US'
    self.someVar = 15325
end

function ChildClass:getStreet()
    return self.street
end

function ChildClass:getSomeVar()
    return self.someVar
end

-- Test the inheritance out ====================================================
Class1 = BaseClass:new()
Class1:init()

Class2 = ChildClass:new()
Class2:init()

print( Class1:getName() )
print( Class2:getStreet() )
print( Class2:getSomeString() )
print( Class2:isa( ChildClass ))
print( Class2:superClass():isa( BaseClass ) )
