local class = require '../Engine/Helpers/middleclass/middleclass' -- OOP lib
local ColTest = class( 'CollisionTest' )
-- Collision is the entry point for all collision methods.
Col = require '../Engine/Physics/Collision'

function ColTest:init()
    -- Create the class members here?
    self.shape1 = {}
    self.shape2 = {}

    -- Other variables that can be used to test here
end
