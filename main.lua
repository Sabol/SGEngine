gfx = love.graphics

require "Engine/Helpers/AnAL"
require "Engine/Physics/World"
require "Engine/Object/Player"
require "Engine/Camera/Camera"
require "Engine/MapLoader/MapLoader"
require "Engine/Editor/Editor"
require "Engine/Sound/Sound"

function love.load()
	-- scale = 1.5
	-- Just leave the scale as is for now.
	scale = 1
	--set the maps
	loader = loader:new()
	loader:init()

	--set up the camera
	cam = camera:new()
	cam:init( 0, 0, scale, 3 )

	--flag for edit mode
	edit = false

	-- Flag for music playback
	play = false 

	--set up the Player
	Player = Player:new()
	Player:init( 200, 200, 16, 32, 'Player' )

	--set up a temp animation
	--currently only has a single frame
	--anim = newAnimation(Player.sprite, 32, 32, 0.1, 0)
	--anim:setMode("loop")

	--create blocks based on the map table
	--[[for y = 1, 23, 1 do
		for x = 1, 30, 1 do
			if loader.levels.level_1[y][x] ~= 0 then
				add_block(x * 16, y * 16, 16, 16)
			end
		end
	end--]]
	loader:create_map( loader.levels.level_1 )

	-- Set up a song
	g_source = create_source( "Game/Assets/sv_ttt.wav" )
	-- play_sound( g_source, true )
end

function love.update( dt )
	Player:update( dt )
	cam:update( dt,Player.x,Player.y,Player.xvel,Player.yvel )
	world_update( dt )

	if play == true then
		g_source:play( true )
	else
		g_source:pause( true )
	end
end

function love.draw()
	debug_disp()
	gfx.scale( scale )
	gfx.push()
	gfx.translate( cam.x, cam.y )
	world_draw( cam )
	Player:draw( "line" )
	flip()
	gfx.pop()
end

function love.keypressed( k )
	if k == "escape" then
		love.event.quit()
	end

	if k == "r" then
		Player:init( 200, 200, 16, 32 )
	end

	if k == "m" then
		if edit == true then
			edit = false
		else
			--enter edit mode
			edit = true
		end
	end

	-- Toggle the music
	if k == "p" then
		if play == true then
			play = false
			g_source:play(true)
		else
			play = true
		end
	end

	if k == "=" then
		scale = scale + 1
	end
	if k == "-" then
		scale = scale - 1
	end
end

function love.keyreleased( k )
	if k == "a" or k == "d" then
		Player.state = "idle"
	end

	if k == "g" then
		Player.jump_released = true
	end
end

function love.mousereleased( x,y,button )
	-- Here be dragons...
	-- g_addX, g_addY, g_addW, g_addH are all global variables,
	-- They can be used as long as Engine/Physics/World is sources.

	-- when the right mouse button is clicked, check to see if it's coord...
	-- intersect a block and if so then remove it from the table of blocks.
	if (button == "r") then
		for i,v in ipairs(blocks) do
			if(CheckCollision(x - cam.x,y - cam.y,1,1,v.x,v.y,v.w,v.h)) then
				table.remove(blocks,i)
			end
		end
	end

	if(button == "l") then
		if(g_addW < 0) then
			g_addX = g_addX + g_addW
			g_addW = -g_addW
		end

		if(g_addH < 0) then
			g_addY = g_addY + g_addH
			g_addH = -g_addH
		end

		if(g_addW ~= 0 and g_addH ~= 0) then
			add_block(g_addX,g_addY,g_addW,g_addH)
		end

		g_addX = 0
		g_addY = 0
	end
end

-- This just detects a collision between two rectangles.
function CheckCollision( ax1,ay1,aw,ah, bx1,by1,bw,bh )
	local ax2,ay2,bx2,by2 = ax1 + aw, ay1 + ah, bx1 + bw, by1 + bh
  	return ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1
end

--flips the Player sprite, will later change to accept any sprite/ animaiton
function flip()
	if Player.facing_right == false then
		Player.current_anim:draw( Player.x + Player.w, Player.y - Player.h/2, 0, -1, 1 )
	elseif Player.facing_right == true then
		Player.current_anim:draw( Player.x - Player.w, Player.y - Player.h/2, 0, 1, 1 )
	end
end

function debug_disp()
	local x = math.floor( Player.x / 16 )
	local y = math.floor( Player.y / 16 )
	gfx.print( "("..x.."), ("..y..")", 16, 16 )

	gfx.print( "press r to reset", 16, 32 )

	if edit == true then
		gfx.print( "edit mode enabled", 16, 48 )
		gfx.print( "block x: "..g_addX.." block y: "..g_addY, 16, 86 )
	end

	gfx.print( "mouse x: "..love.mouse.getX().." mouse y: "..love.mouse.getY(), 16, 64 )
end
