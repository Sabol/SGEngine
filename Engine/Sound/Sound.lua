--[[
Where all the sound code should go.
]]

-- Takes a sound file, sound data, or decoder and
-- creates a new sound source.
function create_source( sound_file )
	local source = love.audio.newSource( sound_file )
	return source
end
-- This is just a temporary deal
function play_sound( source, loop )
	source:setLooping( loop )
	love.audio.play( source )	
end

function pause_all()
	love.audio.pause()
end

function resume_all()
	love.audio.resume()
end