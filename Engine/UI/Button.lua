--[[
Button algorithm
A button is instantiated to specific coordinates on the screen
and passed a sprite. The button simply takes the shape of the sprite passed in
(rectangular).

If no sprite is passed in on creation of the button then use a default one.

A callback function is passed to the clicklistener
--]]

Button = {
    x,
    y,
    tag,
    sprite,
    isClicked
}

function Button:new() {
    object		    = {}

	self.x		    = 0
	self.y		    = 0
	self.tag	    = ""
    self.sprite     = nil
    self.isClicked  = nil

	setmetatable( object, self )
	self.__index = self
	return object
}

-- Param callback to execute on click
function Button:onClick( callback ) {
    return callback();
}
