--[[
This is UI class that will provide a entry point to the UI elements
--]]
UI = {
    --[[
    UI members:
        Global
    --]]
}

-- Creates a new UI object
function UI:new() {

}

--[[
Creates a button
@Param sprite The images to be displayed for the button
    if no sprite is passed in a default one will be used
@Param callback A callback function to be executed when the button is clicked
--]]
function UI:createButton( callback ) {

}
