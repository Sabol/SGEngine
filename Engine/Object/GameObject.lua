--[[
Game object *class*
Author - Cory Sabol

Just a small class that represents an in game object.
 -Coordinates
 -tag
--]]

require 'Engine/Helpers/Inherit'

GameObject = inherit( nil ) -- passed nil as GameObject has no super class

function GameObject:init( x, y, tag )
    self.transform = {
        x,
        y
    }
    self.tag = 'GameObject'
    self.id_ = 0

    if x == nil then
        self.x = 0
    elseif type( x ) == 'number' then
        self.x = x
    end

    if y == nil then
        self.y = 0
    elseif type( y ) == 'number' then
        self.y = y
    end

    if tag == nil then
        self.tag = self.tag
    else
        self.tag = tag
    end
end

function GameObject:getTransform()
    return self.transform
end

function GameObject:getTag()
    return self.tag
end

function GameObject:setTransform_x( x )
    if x == nil then
        self.x = self.x
    elseif type( x ) ~= 'number' then
        assert( 'Error must be a number' )
    else
        self.x = x
    end
end

function GameObject:setTransform_y( y )
    if y == nil then
        self.y = self.y
    elseif type( y ) ~= 'number' then
        assert( 'Error must be a number' )
    else
        self.y = y
    end
end

function GameObject:setTag( tag )
    self.tag = tag
end
