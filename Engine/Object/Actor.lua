--[[
A actor class that extends from the GameObj class.
An actor servers as a base for things like doors, enemies, npc's, or the player.
What should the Actor class have?
Should it perhaps have physics such as velocity.
--]]

require '../Helpers/Inherit'
local GameObject = require './GameObject'

Actor = inherit( GameObject )

function Actor:init()

end
