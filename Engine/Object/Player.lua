--[[
Player class
Author Cory Sabol

TODO: 
    1. Refactor and decouple Player class functionality into various engine modules
    2. Implement crouching (display the crouching sprite/anim and reduce the AABB by 1/3 then shift by that many px down.
]]

require 'Engine/Helpers/Inherit'
require 'Engine/Object/GameObject'

GameObject = GameObject:new()

Player = inherit( GameObject )

--Initialize the Player to the default values
function Player:init( x, y, w, h, tag )
	self.super = self:superClass()

	--print( self.super.getTag() )

    -- Declare / initialize the player member vars
	self.w = w-1
	self.h = h-1
	self.hp = 100
	self.jump_t = 1
	self.jump_t_max = 1
	self.grounded = false
	self.xvel = 0
	self.yvel = 0
	self.xdir = 1
	self.move_speed = 200
	self.move_acc = 500
	self.friction = 1500
	self.gravity = 500
	self.jump_vel = 310 -- Player jumps 6 blocks high at max.
	self.term_vel = 500
	self.sprites = {
		idle = gfx.newImage( "Game/Assets/IdleCycle.png" ),
		run = gfx.newImage( "Game/Assets/RunCycle.png" )
	}
	self.facing_right = true
	self.anims = {
		idle = newAnimation( self.sprites.idle, 32, 32, 0.5, 2 ),
		run = newAnimation( self.sprites.run, 32, 32, 0.1, 8 ),
		jump = nil,
		attack = nil,
		fall = nil,
		death = nil
	}
	self.current_anim = self.anims.idle
	self.state = "idle"
	self.sprites.idle:setFilter( "nearest" )
	self.sprites.run:setFilter( "nearest" )

	self.super:init( x, y, tag )
end

--Update the Players data
function Player:update( dt )
	--Move left
	if love.keyboard.isDown( "a" ) then
		Player:left( dt )
	--Move right
	elseif love.keyboard.isDown( "d" ) then
		Player:right( dt )
	else
		--Apply friction to the velocity only if grounded and no movement keys are pressed
		Player:apply_fric( dt )
	end

	--Apply gravity
	Player:apply_grav( dt )

	--Jump
	Player:jump( dt )

	--Player:jump_timer(dt)

	--This is probably bad practice
	--This is also where I am checking for collisions between boxes
	self.x,
	self.y,
	self.xvel,
	self.yvel,
	self.grounded
	= block_col( self.x, self.y, self.w, self.h, self.grounded, self.xvel, self.yvel, dt )

	--Update the Player coordinates
	self.x = self.x + self.xvel * dt
	self.y = self.y + self.yvel * dt

	Player:anims_update( dt )
end

function Player:anims_update( dt )
	if self.state == "idle" then
		self.current_anim = self.anims.idle
	elseif self.state == "running" and self.grounded == true then
		self.current_anim = self.anims.run
	elseif self.state == "jump" then
		--currently no jump animation
		self.current_anim = self.anims.idle
	end
	self.current_anim:update( dt )
end

function Player:draw( draw_type )
	--This is the Player hit box
	gfx.rectangle( draw_type, self.x - self.w/2, self.y - self.h/2, self.w, self.h )
	if self.state == "attack" then
		gfx.rectangle( "fill", self.x + self.w, self.y - self.h/2, 10, 5 )
	end

	gfx.print( self.tag, self.x - 8, self.y - 32 )
end

--Apply gravity MOVE TO PHYSICS
function Player:apply_grav( dt )
	if self.grounded == false then
		self.yvel = self.yvel + self.gravity * dt
		if self.yvel > self.term_vel then
			self.yvel = self.term_vel
		end
	end
end

--Apply friction MOVE TO PHYSICS
function Player:apply_fric( dt )
	if self.grounded == true then
		if self.xvel > 0 then --If moving right
			self.xvel = self.xvel - self.friction * dt
				if self.xvel < 0 then
					self.xvel = 0
				end
		elseif self.xvel < 0 then --If moving left
			self.xvel = self.xvel + self.friction * dt
			if self.xvel > 0 then
				self.xvel = 0
			end
		end
	end
end

--movements MOVE TO INPUT
function Player:left( dt )
	self.state = "running"
	self.facing_right = false
	if self.xvel > -self.move_speed then
		self.xvel = self.xvel - self.move_acc * dt
	end
	if ( self.xvel < -self.move_speed and self.grounded == true ) then
		self.xvel = -self.move_speed
	end
	if self.xdir == -1 then
		if self.grounded == true then
			self.xvel = -self.xvel/4
		end
		self.xdir = -1
	end
end

-- MOVE TO INPUT MODULE
function Player:right( dt )
	self.state = "running"
	self.facing_right = true
	if self.xvel < self.move_speed then
		self.xvel = self.xvel + self.move_acc * dt
	end
	if self.xvel > self.move_speed and self.grounded == true then
		self.xvel = self.move_speed
	end
	if self.xdir == -1 then
		if self.grounded == true then
			self.xvel = -self.xvel/4
		end
		self.xdir = 1
	end
end

--jump MOVE TO INPUT MODULE
function Player:jump( dt )
	if self.grounded == true and love.keyboard.isDown( " " ) then
		self.jump_released = false
		self.state = "jump"
		self.yvel = -self.jump_vel
		self.grounded = false
	elseif self.jump_released then
		--Check if yvel is < than yvel_max
		if self.yvel < 0 then
			if self.yvel < -200 then
				self.yvel = -200
			end
		end
	end
end

--Jump farther if max xvel is reached MORE THOUGHT NEEDED
-- function Player:long_jump()
-- 	if math.abs( self.xvel ) >= self.move_speed then
-- 		self.xvel = self.xvel * 1.5
-- 	end
-- end
