--[[
Simplistic class to represent bounding boxes
--]]
require '../Helpers/Inherit'

GameObject = require '../Object/GameObject'

Box = inherit( GameObject )

-- @param x The x coordinate of the top left vertex
-- @param y The y coordinate of the top left vertex
-- @param w The width of the box
-- @param h The height of the box
function Box:init( x, y, w, h, tag )
    Box:superClass():init( x, y, tag ) -- initialize superclass

    self.w = w
    self.h = h
    self.points = {}
    self.center = {}
    self.center.x = self.x + self.w / 2
    self.center.y = self.y + self.h / 2

    self.points[0] = { self.x, self.y }
    self.points[1] = { self.x + self.w, self.y }
    self.points[2] = { self.x + self.w, self.y + self.h }
    self.points[3] = { self.x, self.y + self.h }
end

-- Draws the box for debugging purposes
function Box:draw()

end

function Box:getCenter()
    return self.center
end

function Box:getPoints()
    return self.points
end
