-- Shape object
-- A shape is just a collection of points where each point P is a table of size 2
--[[
Shape class
Members:
    points - a table of (x,y) coordinates that represent the shape
    x - the x of the first coordinate pair in points
    y - the y of the first coordinate pair in points
Methods:
    init(points) - Constructor, takes table of points
    get_points() - returns a table of (x,y) points representing the shape
    get_center() - returns a table {x,y} the is the center of the shape
--]]

local class = require '../Helpers/middleclass/middleclass'
local Shape = class( 'Shape' )

function Shape:init( points )
    -- Initialize the shape to the first point
    self.x = points[0][0]
    self.y = points[0][1]
    self.points = points
    self.center = nil -- Need to calculate the center point
end

-- Returns the collections points that represent the shape
function Shape:get_points()
    return self.points
end

-- Returns the (x,y) of the center of the shape
function Shape:get_center()
    return nil
end
