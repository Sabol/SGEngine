--[[ A library for creating 2D vectors --]]

Vector2d = {}

function Vector2d:new()
	local object = {
		x,
		y
	}
	setmetatable( object, {__index = Vector2d} )
	return object
end

function Vector2d:init( x, y )
	self.x = x
	self.y = y
end

-- Add two vectors together.
-- Returns a new Vector2d
function Vector2d:add( v )
	--[[
	<ux, uy, uz> + <vx, vy, vz> = <ux+vx, uy+vy, uz+vx>
	--]]
	local result = self:new()
	result:init( self.x+v.x, self.y+v.y )
	return result
end

-- Multiply a vector by a scalar value
function Vector2d:scalar( scalar )
	self.x = self.x * scalar
	self.y = self.y * scalar
end

-- Calculate the dot product of two vectors.
-- Returns a scalar value
function Vector2d:dot( v )
	--[[
	u dot v = (ux*vx + uy*vy)
	--]]
	return ( self.x*v.x + self.y*v.y )
end

-- Returns a scalar value
function Vector2d:magnitude()
	--[[
	|u| = sqrt(x^2 + y^2)
	--]]
	return math.sqrt( math.pow(self.x, 2) + math.pow(self.y, 2) )
end

-- Project u onto v and return the resulting vector
-- Returns a new Vector2d
function Vector2d:project( v )
	--[[
	project u onto v => (u dot v / v dot v) * v
	--]]
	local proj = self:new()
	local dot = self:dot( v )
	local scalar = self:dot( v ) / v:dot( v )
	proj:init( v.x * scalar, v.y * scalar )

	return proj
end

-- This needs more work.
-- The normal is the vector which is perpendicular a surface
function Vector2d:normal()
	local normal = self:new()
	normal:init( 0, 0 )
	normal.x = self.x - v.x
	normal.y = self.y - v.y

	normal.y = normal.y * -1

	return normal
end

--[[
@param a The starting point of a line segment
@param b The end point of a line segment
--]]
function Vector2d:normal( a, b )
	local normal = self:new()
	normal.x = a.x - b.x
	normal.y = a.y - b.y
	-- Switch the sign of one part of the normal
	normal.y = normal.y * -1
	return normal
end

function Vector2d:unit()
	--[[
	find the unit vector of a vector
	divide each term by the length of the vector
	<ux / |u| , uy / |u|>
	--]]
	local unit = self:new()
	unit.x = self.x / self:magnitude()
	unit.y = self.y / self:magnitude()

	return unit
end
