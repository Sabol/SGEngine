blocks = {} -- Table that holds all the blocks of a level

-- Global variables
g_addX = 0
g_addY = 0
g_addW = 0
g_addH = 0

g_cell_size = 16

function world_update(dt)
	create_block()
end

-- The scaling of the graphics is fucking this shit up.
function create_block()
	if( love.mouse.isDown( "l" ) and edit == true ) then
		if(g_addX == 0) then
			g_addX = (math.floor((love.mouse.getX() + g_cell_size/2 - cam.x)/g_cell_size) * g_cell_size) * scale
			g_addY = (math.floor((love.mouse.getY() + g_cell_size/2 - cam.y)/g_cell_size) * g_cell_size) * scale
		end

		g_addW = math.floor((love.mouse.getX() + g_cell_size/2 - g_addX - cam.x)/g_cell_size) * g_cell_size
		g_addH = math.floor((love.mouse.getY() + g_cell_size/2 - g_addY - cam.y)/g_cell_size) * g_cell_size
	end
end

function draw_block()
	if( love.mouse.isDown( "l" ) and edit == true ) then
		love.graphics.circle( "line",g_addX,g_addY,10 )
		love.graphics.circle( "line",g_addX + g_addW,g_addY + g_addH,10 )
		love.graphics.rectangle( "line",g_addX,g_addY,g_addW,g_addH )
	end
end

function draw_grid( cam )
	gfx.setColor(50, 50, 50, 200)
	for i = 1, cam.screen_w/g_cell_size + 1 do
		local x = math.floor((-cam.x) / g_cell_size) * g_cell_size + i * g_cell_size
		local grid_y = math.floor((-cam.y) / g_cell_size) * g_cell_size
		gfx.line(x, grid_y, x, grid_y + cam.screen_h)
		local y = math.floor((-cam.y) / g_cell_size) * g_cell_size + i * g_cell_size
		local grid_x = math.floor((-cam.x) / g_cell_size) * g_cell_size
		gfx.line(grid_x, y, grid_x + cam.screen_w, y)
	end
	gfx.setColor(255, 255, 255)
end

function draw_blocks( blocks )
	for i,v in ipairs( blocks ) do
		gfx.rectangle( "fill",v.x,v.y,v.w,v.h )
	end
end

function world_draw( cam )
	draw_grid( cam )
	-- This draws the out line of the block being added.
	draw_block()
	-- This draws every block in blocks.
	draw_blocks( blocks )
end

-- This function is fucking hideous.
-- I must have been drunk when I wrote.
-- This detects a collision and calculates the offset of
-- two retangles
-- This needs to be updated to use the separating axis theorem.
-- This also needs to be moves into it's own file/library.
function block_col(x, y, w, h, grounded, dx, dy, dt)
	local on_ground = false
	for i,v in ipairs(blocks) do
		if (x + w/2 > v.x and x - w/2 < v.x + v.w) then
			if (y - h/2 > v.y + v.h and y - h/2 + dy * dt < v.y + v.h) then
				y = v.y + v.h + h/2
				dy = 0
			end
			if (y + h/2 < v.y and y + h/2 + dy * dt > v.y) then
				y = v.y - h/2
				dy = 0
				grounded = true
			end
		end

		if (y + h/2 > v.y and y - h/2 < v.y + v.h) then
			if (x + w/2 < v.x and x + w/2 + dx * dt > v.x or (dx > 1 and x + w/2 == v.x)) then
				x = v.x - w/2
				dx = 0
			end
			if (x - w/2 > v.x + v.w and x - w/2 + dx * dt < v.x + v.w or (x - w/2 == v.x + v.w and dx < 0)) then
				x = v.x + v.w + w/2
				dx = 0
			end
		end

		if (grounded == true and y + h/2 == v.y and x + w/2 > v.x and x - w/2 < v.x + v.w) then
			on_ground = true
		end
	end

	if (on_ground == true) then
		grounded = true
	else
		grounded = false
	end

	return x,y,dx,dy,grounded
end

--Add a block to the world for collision
function add_block( x,y,w,h )
	local block = {}
	block.x = x
	block.y = y
	block.w = w
	block.h = h

	--solid = true

	table.insert(blocks, block)
end
