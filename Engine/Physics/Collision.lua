--[[
Collision detection library.
Author - Cory Sabol

A small collision detection library to caculate collisions between game
objects.

This utilizes the separating axis theorem which states that
 “If two convex objects are not penetrating, there exists an axis for which the projection of the objects will not overlap.”
--]]

--[[
Need to get just the blocks surrounding the player and check only those for
collisions with the player.

Need to actually figure out a more efficient collision algorithm...
--]]

require '../Helpers/Inherit'

local Shape = require './Shape'
local Vector2D = require './Vector2d'

Collision = inherit( nil ) -- pass nill because no super class

-- Begin the broad phase of SAT (to be implemented later)
function Collison:broad_phase(  )

end

-- Begin the narrow phase of SAT
function Collision:narrow_phase(  )

end

-- This uses SAT to check collision between two shapes.
function Collision:check_collison( object_1, object_2 )
	--[[
	This function should compare two objects for collision.
	Should set the values of the fist object.

	THIS NEEDS MORE THOUGHT.
	--]]

end

-- Need to go through each shape and collect all the axes to project to.
function Collision:get_axes( shape )
	axes = {}

	--[[
	a shape is represented by a collection of points
	Assume everything is just a rectangle for now...
	--]]

end

function Collision:project_to_x()
	-- body
end

function Collision:project_to_y()
	-- body
end
