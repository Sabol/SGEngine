--A camera class by Cory Sabol *license shit here*
camera = {}

function camera:new()
	local object = {
		x,
		y,
		tx,
		ty,
		screen_w,
		screen_h,
		scale,
		speed
	}
	setmetatable(object, {__index = camera})
	return object
end

function camera:init(x, y, scale, speed)
	self.x = x or 0
	self.y = y or 0
	self.tx = 0
	self.ty = 0
	self.screen_w = love.window.getWidth()
	self.screen_h = love.window.getHeight()
	self.scale = scale
	self.speed = speed
end

function camera:update(dt, p_x, p_y, dx, dy)
	self.tx = -(p_x + dx) + self.screen_w/2 / self.scale 
	self.ty = -p_y + -dy/2 + self.screen_h/(2 * self.scale)

	self.x = self.x + ((self.tx - self.x) * self.speed * dt)
	self.y = self.y + ((self.ty - self.y) * self.speed * dt)
end

function camera:scale(scale)
	self.scale = scale
end
