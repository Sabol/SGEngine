--[[
This class handles all the input for the player and for NPC's.
This class could also listen for data from the AI module
Author - Cory Sabol 2015
Liscence stuff here
--]]

class = require '../Helpers/middleclass/middleclass'
Input = class( 'Input' )

-- @param actor The actor object to be moved based on input
function Input:init( actor )
    -- Do some initialization stuff here
end
