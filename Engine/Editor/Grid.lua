--[[
A small grid class
--]]

local class = require '../Helpers/middleclass/middleclass'
local Grid = class( 'Grid' )

function Grid:init()

end

function Grid:draw_grid( cam )
	gfx.setColor(50, 50, 50, 200)
	for i = 1, cam.screen_w/g_cell_size + 1 do
		local x = math.floor((-cam.x) / g_cell_size) * g_cell_size + i * g_cell_size
		local grid_y = math.floor((-cam.y) / g_cell_size) * g_cell_size
		gfx.line(x, grid_y, x, grid_y + cam.screen_h)
		local y = math.floor((-cam.y) / g_cell_size) * g_cell_size + i * g_cell_size
		local grid_x = math.floor((-cam.x) / g_cell_size) * g_cell_size
		gfx.line(grid_x, y, grid_x + cam.screen_w, y)
	end
	gfx.setColor(255, 255, 255)
end
